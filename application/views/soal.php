<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>To see or not to see</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url() . "assets/vendor/bootstrap/css/bootstrap.min.css" ?>">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url() . "assets/css/freelancer.css" ?>">

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="<?php echo base_url() . "assets/vendor/font-awesome/css/font-awesome.min.css" ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
     <!-- jQuery -->
    <script src="<?php echo base_url() . "assets/vendor/jquery/jquery.min.js" ?>"></script>

</head>

<body id="page-top" class="index">
<div id="skipnav"><a href="#maincontent">Skip to main content</a></div>

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="<?php echo base_url() . "index.php/welcome/index" ?>">To see or not to see</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header style="background-color:#fff; color:#000;">
        <div class="container" >
                <div class="row" style="margin-top:-80px;">
                <div class="col-lg-12" id="soal1">
                    <h1>Perhatikan Gambar di bawah</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div style="border:1px solid black;" >
                            <!-- <img src="<?php echo base_url() . "img/soal/slide1.png" ?>" class="img-responsive"> -->
                            <?php 
                                foreach($soal as $res){
                                    if($res['urutan'] == 1){
                                        echo "<img src='".$res['soal']."' class='img-responsive'>";
                                    }
                                }
                            ?>
                            <div style="display:none;">
                                <input type="text" id="timer1" />
                                <input id="stop1" type="button" value="stop"/>
                                <input id="start1" type="button" value="start"/>
                            </div>
                            
                        </div>
                        <br />
                        <button class="btn btn-warning" id="next1">Next</button>
                        <script type="text/javascript">
                        $( document ).ready(function() {
                            $('#next1').focus();
                        });
                        </script>
                    </div>
                </div>
                <div class="col-lg-12" id="jawab1" style="display:none;">
                    <h1>Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <label>Apa makna dari rambu tersebut ?</label><br/>
                        <div class="form-group col-xs-12">
                             <input type="text" class="form-control" placeholder="" id="jawaban1">
                        </div>
                        <button class="btn btn-warning" id="jwb1">Submit</button>                    
                    </div>
                </div>
                <div class="col-lg-12" id="soal2" style="display:none;">
                    <h1>Perhatikan Gambar di bawah</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div style="border:1px solid black;" >
                            <!-- <img src="<?php echo base_url() . "img/soal/slide1.png" ?>" class="img-responsive"> -->
                            <?php 
                                foreach($soal as $res){
                                    if($res['urutan'] == 2){
                                        echo "<img src='".$res['soal']."' class='img-responsive'>";
                                    }
                                }
                            ?>
                            <div style="display:none;">
                                <input type="text" id="timer2" />
                                <input id="stop2" type="button" value="stop"/>
                                <input id="start2" type="button" value="start"/>
                            </div>
                        </div>
                        <br />
                        <button class="btn btn-warning" id="next2">Next</button>
                    </div>
                </div>
                <div class="col-lg-12" id="jawab2" style="display:none;">
                    <h1>Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <label>Apa makna dari rambu tersebut ?</label><br/>
                        <div class="form-group col-xs-12">
                             <input type="text" class="form-control" placeholder="" id="jawaban2">
                        </div>
                        <button class="btn btn-warning" id="jwb2">Submit</button>
                    </div>
                </div>
                <div class="col-lg-12" id="soal3" style="display:none;">
                    <h1>Perhatikan Gambar di bawah</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div style="border:1px solid black;" >
                            <!-- <img src="<?php echo base_url() . "img/soal/slide1.png" ?>" class="img-responsive"> -->
                            <?php 
                                foreach($soal as $res){
                                    if($res['urutan'] == 3){
                                        echo "<img src='".$res['soal']."' class='img-responsive'>";
                                    }
                                }
                            ?>
                            <div style="display:none;">
                                <input type="text" id="timer3" />
                                <input id="stop3" type="button" value="stop"/>
                                <input id="start3" type="button" value="start"/>
                            </div>
                        </div>
                        <br />
                        <button class="btn btn-warning" id="next3">Next</button>
                    </div>
                </div>
                <div class="col-lg-12" id="jawab3" style="display:none;">
                    <h1>Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <label>Apa makna dari rambu tersebut ?</label><br/>
                        <div class="form-group col-xs-12">
                             <input type="text" class="form-control" placeholder="" id="jawaban3">
                        </div>
                        <button class="btn btn-warning" id="jwb3">Submit</button>
                    </div>
                </div>
                <div class="col-lg-12" id="soal4" style="display:none;">
                    <h1>Perhatikan Gambar di bawah</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div style="border:1px solid black;" >
                            <!-- <img src="<?php echo base_url() . "img/soal/slide1.png" ?>" class="img-responsive"> -->
                            <?php 
                                foreach($soal as $res){
                                    if($res['urutan'] == 4){
                                        echo "<img src='".$res['soal']."' class='img-responsive'>";
                                    }
                                }
                            ?>
                            <div style="display:none;">
                                <input type="text" id="timer4" />
                                <input id="stop4" type="button" value="stop"/>
                                <input id="start4" type="button" value="start"/>
                            </div>
                        </div>
                        <br />
                        <button class="btn btn-warning" id="next4">Next</button>
                    </div>
                </div>
                <div class="col-lg-12" id="jawab4" style="display:none;">
                    <h1>Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <label>Apa makna dari rambu tersebut ?</label><br/>
                        <div class="form-group col-xs-12">
                             <input type="text" class="form-control" placeholder="" id="jawaban4">
                        </div>
                        <button class="btn btn-warning" id="jwb4">Submit</button>
                    </div>
                </div>
                <div class="col-lg-12" id="soal5" style="display:none;">
                    <h1>Perhatikan Gambar di bawah</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div style="border:1px solid black;" >
                            <!-- <img src="<?php echo base_url() . "img/soal/slide1.png" ?>" class="img-responsive"> -->
                            <?php 
                                foreach($soal as $res){
                                    if($res['urutan'] == 5){
                                        echo "<img src='".$res['soal']."' class='img-responsive'>";
                                    }
                                }
                            ?>
                            <div style="display:none;">
                                <input type="text" id="timer5" />
                                <input id="stop5" type="button" value="stop"/>
                                <input id="start5" type="button" value="start"/>
                            </div>
                        </div>
                        <br />
                        <button class="btn btn-warning" id="next5">Next</button>
                    </div>
                </div>
                <div class="col-lg-12" id="jawab5" style="display:none;">
                    <h1>Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <label>Apa makna dari rambu tersebut ?</label><br/>
                        <div class="form-group col-xs-12">
                             <input type="text" class="form-control" placeholder="" id="jawaban5">
                        </div>
                        <button class="btn btn-warning" id="jwb5">Submit</button>
                    </div>
                </div>
                <div class="col-lg-12" id="soal6" style="display:none;">
                    <h1>Perhatikan Gambar di bawah</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div style="border:1px solid black;" >
                            <!-- <img src="<?php echo base_url() . "img/soal/slide1.png" ?>" class="img-responsive"> -->
                            <?php 
                                foreach($soal as $res){
                                    if($res['urutan'] == 6){
                                        echo "<img src='".$res['soal']."' class='img-responsive'>";
                                    }
                                }
                            ?>
                            <div style="display:none;">
                                <input type="text" id="timer6" />
                                <input id="stop6" type="button" value="stop"/>
                                <input id="start6" type="button" value="start"/>
                            </div>
                        </div>
                        <br />
                        <button class="btn btn-warning" id="next6">Next</button>
                    </div>
                </div>
                <div class="col-lg-12" id="jawab6" style="display:none;">
                    <h1>Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <label>Apa makna dari rambu tersebut ?</label><br/>
                        <div class="form-group col-xs-12">
                             <input type="text" class="form-control" placeholder="" id="jawaban6">
                        </div>
                        <button class="btn btn-warning" id="jwb6">Submit</button>
                    </div>
                </div>
                <div class="col-lg-12" id="soal7" style="display:none;">
                    <h1>Perhatikan Gambar di bawah</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div style="border:1px solid black;" >
                            <!-- <img src="<?php echo base_url() . "img/soal/slide1.png" ?>" class="img-responsive"> -->
                            <?php 
                                foreach($soal as $res){
                                    if($res['urutan'] == 7){
                                        echo "<img src='".$res['soal']."' class='img-responsive'>";
                                    }
                                }
                            ?>
                            <div style="display:none;">
                                <input type="text" id="timer7" />
                                <input id="stop7" type="button" value="stop"/>
                                <input id="start7" type="button" value="start"/>
                            </div>
                        </div>
                        <br />
                        <button class="btn btn-warning" id="next7">Next</button>
                    </div>
                </div>
                <div class="col-lg-12" id="jawab7" style="display:none;">
                    <h1>Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <label>Apa makna dari rambu tersebut ?</label><br/>
                        <div class="form-group col-xs-12">
                             <input type="text" class="form-control" placeholder="" id="jawaban7">
                        </div>
                        <button class="btn btn-warning" id="jwb7">Submit</button>
                    </div>
                </div>
                <div class="col-lg-12" id="soal8" style="display:none;">
                    <h1>Perhatikan Gambar di bawah</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div style="border:1px solid black;" >
                            <!-- <img src="<?php echo base_url() . "img/soal/slide1.png" ?>" class="img-responsive"> -->
                            <?php 
                                foreach($soal as $res){
                                    if($res['urutan'] == 8){
                                        echo "<img src='".$res['soal']."' class='img-responsive'>";
                                    }
                                }
                            ?>
                            <div style="display:none;">
                                <input type="text" id="timer8" />
                                <input id="stop8" type="button" value="stop"/>
                                <input id="start8" type="button" value="start"/>
                            </div>
                        </div>
                        <br />
                        <button class="btn btn-warning" id="next8">Next</button>
                    </div>
                </div>
                <div class="col-lg-12" id="jawab8" style="display:none;">
                    <h1>Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <label>Apa makna dari rambu tersebut ?</label><br/>
                        <div class="form-group col-xs-12">
                             <input type="text" class="form-control" placeholder="" id="jawaban8">
                        </div>
                        <button class="btn btn-warning" id="jwb8">Submit</button>
                    </div>
                </div>
                <div class="col-lg-12" id="soal9" style="display:none;">
                    <h1>Perhatikan Gambar di bawah</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div style="border:1px solid black;" >
                            <!-- <img src="<?php echo base_url() . "img/soal/slide1.png" ?>" class="img-responsive"> -->
                            <?php 
                                foreach($soal as $res){
                                    if($res['urutan'] == 9){
                                        echo "<img src='".$res['soal']."' class='img-responsive'>";
                                    }
                                }
                            ?>
                            <div style="display:none;">
                                <input type="text" id="timer9" />
                                <input id="stop9" type="button" value="stop"/>
                                <input id="start9" type="button" value="start"/>
                            </div>
                        </div>
                        <br />
                        <button class="btn btn-warning" id="next9">Next</button>
                    </div>
                </div>
                <div class="col-lg-12" id="jawab9" style="display:none;">
                    <h1>Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <label>Apa makna dari rambu tersebut ?</label><br/>
                        <div class="form-group col-xs-12">
                             <input type="text" class="form-control" placeholder="" id="jawaban9">
                        </div>
                        <button class="btn btn-warning" id="jwb9">Submit</button>
                    </div>
                </div>
                <div class="col-lg-12" id="soal10" style="display:none;">
                    <h1>Perhatikan Gambar di bawah</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div style="border:1px solid black;" >
                            <!-- <img src="<?php echo base_url() . "img/soal/slide1.png" ?>" class="img-responsive"> -->
                            <?php 
                                foreach($soal as $res){
                                    if($res['urutan'] == 10){
                                        echo "<img src='".$res['soal']."' class='img-responsive'>";
                                    }
                                }
                            ?>
                            <div style="display:none;">
                                <input type="text" id="timer10" />
                                <input id="stop10" type="button" value="stop"/>
                                <input id="start10" type="button" value="start"/>
                            </div>
                        </div>
                        <br />
                        <button class="btn btn-warning" id="next10">Next</button>
                    </div>
                </div>
                <div class="col-lg-12" id="jawab10" style="display:none;">
                    <h1>Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <label>Apa makna dari rambu tersebut ?</label><br/>
                        <div class="form-group col-xs-12">
                             <input type="text" class="form-control" placeholder="" id="jawaban10">
                        </div>
                        <button class="btn btn-warning" id="jwb10">Submit</button>
                    </div>
                </div>
                
                <!-- KUESIONER -->

                <div class="col-lg-12" id="q" style="display:none; text-align:left !important;">
                    <h1 style="text-align:center;">Jawab Pertanyaan di bawah Ini</h1>
                    <br /><br />
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="row control-group">
                            <label>Apakah Anda pernah melihat rambu-rambu tersebut?</label><br/>
                            <div class="form-group col-xs-12">
                                <label class="radio-inline" style="margin-top:15px">
                                    <input type="radio" value="0" name="optradio">Ya
                                </label>
                                <label class="radio-inline" style="margin-top:15px">
                                    <input type="radio" value="1" name="optradio">Tidak
                                </label>
                            </div>
                        </div>
                        <div class="row control-group">
                            <label>Jika pernah melihat, apakah Anda memahami rambu yang Anda lihat ?</label><br/>
                            <div class="form-group col-xs-12">
                                <label class="radio-inline" style="margin-top:15px">
                                    <input type="radio" value="0" name="optradio1">Ya
                                </label>
                                <label class="radio-inline" style="margin-top:15px">
                                    <input type="radio" value="1" name="optradio1">Tidak
                                </label>
                            </div>
                        </div>
                        <div class="row control-group">
                            <label>Jika tidak memahami, kenapa Anda tidak memahaminya ? (boleh lebih dari satu)</label><br/>
                            <div class="form-group col-xs-12" style="margin-left:15px;">
                                <label class="checkbox">
                                  <input type="checkbox" name="chkbx" value="Sering melihat tapi tidak mengerti maksud dari rambu tersebut">Sering melihat tapi tidak mengerti maksud dari rambu tersebut
                                </label>
                                <label class="checkbox">
                                  <input type="checkbox" name="chkbx" value="Simbol rambu tidak jelas">Simbol rambu tidak jelas
                                </label>
                                <label class="checkbox">
                                  <input type="checkbox" name="chkbx" value="Tulisan tidak terbaca">Tulisan tidak terbaca
                                </label>
                                <label class="checkbox">
                                  <input type="checkbox" name="chkbx" value="Warna rambu kurang mencolok">Warna rambu kurang mencolok
                                </label>
                                <label class="checkbox">
                                  <input type="checkbox" name="chkbx" value="oth">Lain - Lain <input type="text" class="form-inline" style="width:50%;" id="qoth"/>
                                </label>
                            </div>
                        </div>
                        <button class="btn btn-warning" id="jwb">Kirim Jawaban</button>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- jQuery -->
    <script src="<?php echo base_url() . "assets/vendor/jquery/jquery.min.js" ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url() . "assets/vendor/bootstrap/js/bootstrap.min.js" ?>"></script>

    <!-- Plugin JavaScript -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> -->
    <script src="<?php echo base_url() . "assets/vendor/jquery/jquery.easing.min.js" ?>"></script>

    <!-- Contact Form JavaScript -->
    <script src="<?php echo base_url() . "assets/js/jqBootstrapValidation.js" ?>"></script>
    <script src="<?php echo base_url() . "assets/js/contact_me.js" ?>"></script>

    <!-- Theme JavaScript -->
    <script src="<?php echo base_url() . "assets/js/freelancer.min.js" ?>"></script>

    <script type="text/javascript">
        $( document ).ready(function() {
            var BASE_URL = "<?php echo base_url(); ?>";

            var timer = null, 
            interval = 1,
            value = 0;

            $("#start1").click(function() {
                value = 0;
              if (timer !== null) return;
              timer = setInterval(function () {
                  value = value+1;
                  $("#timer1").val(value);
              }, interval); 
            });

            $("#start1").click();

            $("#stop1").click(function() {
              clearInterval(timer);
              timer = null
            });

            $("#start2").click(function() {
                value = 0;
              if (timer !== null) return;
              timer = setInterval(function () {
                  value = value+1;
                  $("#timer2").val(value);
              }, interval); 
            });

            $("#stop2").click(function() {
              clearInterval(timer);
              timer = null
            });

            $("#start3").click(function() {
                value = 0;
              if (timer !== null) return;
              timer = setInterval(function () {
                  value = value+1;
                  $("#timer3").val(value);
              }, interval); 
            });

            $("#stop3").click(function() {
              clearInterval(timer);
              timer = null
            });

            $("#start4").click(function() {
                value = 0;
              if (timer !== null) return;
              timer = setInterval(function () {
                  value = value+1;
                  $("#timer4").val(value);
              }, interval); 
            });

            $("#stop4").click(function() {
              clearInterval(timer);
              timer = null
            });

            $("#start5").click(function() {
                value = 0;
              if (timer !== null) return;
              timer = setInterval(function () {
                  value = value+1;
                  $("#timer5").val(value);
              }, interval); 
            });

            $("#stop5").click(function() {
              clearInterval(timer);
              timer = null
            });

            $("#start6").click(function() {
                value = 0;
              if (timer !== null) return;
              timer = setInterval(function () {
                  value = value+1;
                  $("#timer6").val(value);
              }, interval); 
            });

            $("#stop6").click(function() {
              clearInterval(timer);
              timer = null
            });

            $("#start7").click(function() {
                value = 0;
              if (timer !== null) return;
              timer = setInterval(function () {
                  value = value+1;
                  $("#timer7").val(value);
              }, interval); 
            });

            $("#stop7").click(function() {
              clearInterval(timer);
              timer = null
            });

            $("#start8").click(function() {
                value = 0;
              if (timer !== null) return;
              timer = setInterval(function () {
                  value = value+1;
                  $("#timer8").val(value);
              }, interval); 
            });

            $("#stop8").click(function() {
              clearInterval(timer);
              timer = null
            });

            $("#start9").click(function() {
                value = 0;
              if (timer !== null) return;
              timer = setInterval(function () {
                  value = value+1;
                  $("#timer9").val(value);
              }, interval); 
            });

            $("#stop9").click(function() {
              clearInterval(timer);
              timer = null
            });

            $("#start10").click(function() {
                value = 0;
              if (timer !== null) return;
              timer = setInterval(function () {
                  value = value+1;
                  $("#timer10").val(value);
              }, interval); 
            });

            $("#stop10").click(function() {
              clearInterval(timer);
              timer = null
            });

            $("#jawab1").keyup(function(event){
                if(event.keyCode == 13){
                    if($('#jawaban1').val() != ""){
                        $("#jwb1").click();
                    }
                }
            });
            $("#jawab2").keyup(function(event){
                if(event.keyCode == 13){
                    if($('#jawaban2').val() != ""){
                        $("#jwb2").click();
                    }
                }
            });
            $("#jawab3").keyup(function(event){
                if(event.keyCode == 13){
                    if($('#jawaban3').val() != ""){
                        $("#jwb3").click();
                    }
                }
            });
            $("#jawab4").keyup(function(event){
                if(event.keyCode == 13){
                    if($('#jawaban4').val() != ""){
                        $("#jwb4").click();
                    }
                }
            });
            $("#jawab5").keyup(function(event){
                if(event.keyCode == 13){
                    if($('#jawaban5').val() != ""){
                        $("#jwb5").click();
                    }
                }
            });
            $("#jawab6").keyup(function(event){
                if(event.keyCode == 13){
                    if($('#jawaban6').val() != ""){
                        $("#jwb6").click();
                    }
                }
            });
            $("#jawab7").keyup(function(event){
                if(event.keyCode == 13){
                    if($('#jawaban7').val() != ""){
                        $("#jwb7").click();
                    }
                }
            });
            $("#jawab8").keyup(function(event){
                if(event.keyCode == 13){
                    if($('#jawaban8').val() != ""){
                        $("#jwb8").click();
                    }
                }
            });
            $("#jawab9").keyup(function(event){
                if(event.keyCode == 13){
                    if($('#jawaban9').val() != ""){
                        $("#jwb9").click();
                    }
                }
            });
            $("#jawab10").keyup(function(event){
                if(event.keyCode == 13){
                   if($('#jawaban10').val() != ""){
                        $("#jwb10").click();
                    }
                }
            });
            $('#next1').click(function(){
                $('#soal1').hide("fast");
                $("#stop1").click();
                $('#jawab1').show("fast");
                $('#jawaban1').focus();
            });
            $('#next2').click(function(){
                $('#soal2').hide("fast");
                $("#stop2").click();
                $('#jawab2').show("fast");
                $('#jawaban2').focus();
            });
            $('#next3').click(function(){
                $('#soal3').hide("fast");
                $("#stop3").click();
                $('#jawab3').show("fast");
                $('#jawaban3').focus();
            });
            $('#next4').click(function(){
                $('#soal4').hide("fast");
                $("#stop4").click();
                $('#jawab4').show("fast");
                $('#jawaban4').focus();
            });
            $('#next5').click(function(){
                $('#soal5').hide("fast");
                $('#stop5').hide("fast");
                $('#jawab5').show("fast");
                $('#jawaban5').focus();
            });
            $('#next6').click(function(){
                $('#soal6').hide("fast");
                $("#stop6").click();
                $('#jawab6').show("fast");
                $('#jawaban6').focus();
            });
            $('#next7').click(function(){
                $('#soal7').hide("fast");
                $('#stop7').hide("fast");
                $('#jawab7').show("fast");
                $('#jawaban7').focus();
            });
            $('#next8').click(function(){
                $('#soal8').hide("fast");
                $("#stop8").click();
                $('#jawab8').show("fast");
                $('#jawaban8').focus();
            });
            $('#next9').click(function(){
                $('#soal9').hide("fast");
                $("#stop9").click();
                $('#jawab9').show("fast");
                $('#jawaban9').focus();
            });
            $('#next10').click(function(){
                $('#soal10').hide("fast");
                $("#stop10").click();
                $('#jawab10').show("fast");
                $('#jawaban10').focus();
            });
            $('#jwb1').click(function(){
                $('#jawab1').hide("fast");
                $("#start2").click();
                $('#soal2').show("fast");
                $('#next2').focus();
            });
            $('#jwb2').click(function(){
                $('#jawab2').hide("fast");
                $("#start3").click();
                $('#soal3').show("fast");
                $('#next3').focus();
            });
            $('#jwb3').click(function(){
                $('#jawab3').hide("fast");
                $("#start4").click();
                $('#soal4').show("fast");
                $('#next4').focus();
            });
            $('#jwb4').click(function(){
                $('#jawab4').hide("fast");
                $("#start5").click();
                $('#soal5').show("fast");
                $('#next5').focus();
            });
            $('#jwb5').click(function(){
                $('#jawab5').hide("fast");
                $("#start6").click();
                $('#soal6').show("fast");
                $('#next6').focus();
            });
            $('#jwb6').click(function(){
                $('#jawab6').hide("fast");
                $("#start7").click();
                $('#soal7').show("fast");
                $('#next7').focus();
            });
            $('#jwb7').click(function(){
                $('#jawab7').hide("fast");
                $('#start8').show("fast");
                $('#soal8').show("fast");
                $('#next8').focus();
            });
            $('#jwb8').click(function(){
                $('#jawab8').hide("fast");
                $("#start9").click();
                $('#soal9').show("fast");
                $('#next9').focus();
            });
            $('#jwb9').click(function(){
                $('#jawab9').hide("fast");
                $("#start10").click();
                $('#soal10').show("fast");
                $('#next10').focus();
            });
            // $('#jwb10').click(function(){
            //     $('#jawab10').hide("fast");
            //     $('#q').show("fast");
            // });
            $("#jwb10").click(function(){
                var soal1 = $("#jawaban1").val();
                var waktu1 = $("#timer1").val();
                var soal2 = $("#jawaban2").val();
                var waktu2 = $("#timer2").val();
                var soal3 = $("#jawaban3").val();
                var waktu3 = $("#timer3").val();
                var soal4 = $("#jawaban4").val();
                var waktu4 = $("#timer4").val();
                var soal5 = $("#jawaban5").val();
                var waktu5 = $("#timer5").val();
                var soal6 = $("#jawaban6").val();
                var waktu6 = $("#timer6").val();
                var soal7 = $("#jawaban7").val();
                var waktu7 = $("#timer7").val();
                var soal8 = $("#jawaban8").val();
                var waktu8 = $("#timer8").val();
                var soal9 = $("#jawaban9").val();
                var waktu9 = $("#timer9").val();
                var soal10 = $("#jawaban10").val();
                var waktu10 = $("#timer10").val();
                var q1 = $('input[name=optradio]:checked').val();
                var q2 = $('input[name=optradio1]:checked').val();
                // $('input:checkbox[name=chkbx]').each(function() 
                // {    
                //     if($(this).is(':checked'))
                //       var q3 = $(this).val();
                // });

                // console.log(q3);


                var uri = BASE_URL + "index.php/welcome/jawab";

                $.ajax({
                    url : uri,
                    type: "POST",
                    data:{
                        soal1:soal1,
                        waktu1:waktu1,
                        soal2:soal2,
                        waktu2:waktu2,
                        soal3:soal3,
                        waktu3:waktu3,
                        soal4:soal4,
                        waktu4:waktu4,
                        soal5:soal5,
                        waktu5:waktu5,
                        soal6:soal6,
                        waktu6:waktu6,
                        soal7:soal7,
                        waktu7:waktu7,
                        soal8:soal8,
                        waktu8:waktu8,
                        soal9:soal9,
                        waktu9:waktu9,
                        soal10:soal10,
                        waktu10:waktu10
                    },
                    dataType : 'json',
                    success: function(msg){
                        alert("Terima Kasih atas kesediaannya mengisi kuesioner ini.");
                        window.location.href = BASE_URL; 
                        // console.log(msg);
                        // location.reload();
                        // $("#add").bPopup().close();
                    },
                    error:function(jqXHR,textStatus, errorThrown) {
                        alert("Error !!!");
                    }
                });
            });
        });
    </script>

</body>

</html>