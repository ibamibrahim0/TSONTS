<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>To see or not to see</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url() . "assets/vendor/bootstrap/css/bootstrap.min.css" ?>">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url() . "assets/css/freelancer.css" ?>">

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="<?php echo base_url() . "assets/vendor/font-awesome/css/font-awesome.min.css" ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
<div id="skipnav"><a href="#maincontent">Skip to main content</a></div>

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#page-top">To see or not to see</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#portfolio">Our Team</a>
                    </li>
                    <!-- <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact">Contact</a>
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container" id="maincontent" tabindex="-1">
            <div class="row" style="margin-top:-100px;">
                <div class="col-lg-12">
                    <!-- <img class="img-responsive" src="<?php echo base_url()."assets/img/profile.png" ?>" alt=""> -->
                    <div class="intro-text" style="margin-bottom:30px;">
                        <h1>form registrasi</h1>
                    </div>

                    <div class="col-lg-8 col-lg-offset-2">
                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                        <form action="<?php echo base_url(); ?>index.php/welcome/pick" onsubmit="return validateForm()" method="post" name="loginForm">
                            <div class="row control-group">
                                <div class="form-group col-xs-12">
                                    <div class="form-group col-xs-6">
                                        <span align="left" style="font-size:1.75em;">
                                            Inisial Nama
                                        </span>
                                    </div>
                                    <div class="form-group col-xs-6">
                                         <input type="text" class="form-control" placeholder="" name="name">
                                    </div>
                                </div>
                            </div>
                            <div class="row control-group">
                                <div class="form-group col-xs-12">
                                    <div class="form-group col-xs-6">
                                        <span align="left" style="font-size:1.75em;">
                                            Jurusan
                                        </span>
                                    </div>
                                    <div class="form-group col-xs-6">
                                         <input type="text" class="form-control" placeholder="" name="jurusan">
                                    </div>
                                </div>
                            </div>
                            <div class="row control-group">
                                <div class="form-group col-xs-12">
                                    <div class="form-group col-xs-6">
                                        <span style="font-size:1.75em">
                                            Institusi
                                        </span>
                                    </div>
                                    <div class="form-group col-xs-6">
                                         <input type="text" class="form-control" placeholder="" name="institusi">
                                    </div>
                                </div>
                            </div>
                            <div class="row control-group">
                                <div class="form-group col-xs-12">
                                    <div class="form-group col-xs-6">
                                        <span style="font-size:1.75em">
                                            Jenis Kelamin
                                        </span>
                                    </div>
                                    <div class="form-group col-xs-6">
                                         <select class="form-control" name="jk">
                                            <option value="lk">Laki-Laki</option>
                                            <option value="pr">Perempuan</option>
                                         </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row control-group">
                                <div class="form-group col-xs-12">
                                    <div class="form-group col-xs-6">
                                        <span align="left" style="font-size:1.75em;">
                                            Apakah Anda Memiliki SIM C?
                                        </span>
                                    </div>
                                    <div class="form-group col-xs-6" style="text-align:left !important; vertical-align:baseline!important;">
                                        <label class="radio-inline" style="margin-top:15px">
                                            <input type="radio" value="0" name="optradio">Ya
                                        </label>
                                        <label class="radio-inline" style="margin-top:15px">
                                            <input type="radio" value="1"  name="optradio">Tidak
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row control-group">
                                <div class="form-group col-xs-12">
                                    <div class="form-group col-xs-6">
                                        <span align="left" style="font-size:1.75em;">
                                            Pengalaman mengendara (Sejak Memiliki SIM)
                                        </span>
                                    </div>
                                    <div class="form-group col-xs-6" style="text-align:left !important; vertical-align:middle!important;">
                                        <label class="radio-inline" style="margin-top:25px">
                                            <input type="radio" value="0" name="optradio1">Kurang dari 1 Tahun
                                        </label>
                                        <label class="radio-inline" style="margin-top:25px">
                                            <input type="radio" value="1" name="optradio1">Lebih dari 1 Tahun
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row control-group">
                                <div class="form-group col-xs-12">
                                    <div class="form-group col-xs-6">
                                        <span align="left" style="font-size:1.75em;">
                                            Frekuensi Berkendara Dalam 1 Minggu
                                        </span>
                                    </div>
                                    <div class="form-group col-xs-6" style="text-align:left !important; vertical-align:middle!important;">
                                        <label class="radio-inline" style="margin-top:25px">
                                            <input type="radio" value="0" name="optradio2">Kurang dari 5 Kali
                                        </label>
                                        <label class="radio-inline" style="margin-top:25px">
                                            <input type="radio" value="1" name="optradio2">Lebih dari atau 5 Kali
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div id="success"></div>
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <!-- <a href="<?php echo base_url() . "index.php/welcome/pick" ?>"> -->
                                    <button type="submit" class="btn btn-success btn-lg">Next</button>
                                    <!-- </a> -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Our Team</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 portfolio-item">
                        <img src="<?php echo base_url()."img/team/1.jpg" ?>" class="img-responsive">
                </div>
                <div class="col-sm-4 portfolio-item">
                        <img src="<?php echo base_url()."img/team/2.jpg" ?>" class="img-responsive">
                </div>
                <div class="col-sm-4 portfolio-item">
                        <img src="<?php echo base_url()."img/team/3.jpg" ?>" class="img-responsive">
                </div>
                <div class="col-sm-4 portfolio-item">
                        <img src="<?php echo base_url()."img/team/4.jpg" ?>" class="img-responsive">
                </div>
                <div class="col-sm-4 portfolio-item">
                        <img src="<?php echo base_url()."img/team/5.jpg" ?>" class="img-responsive">
                </div>
                <div class="col-sm-4 portfolio-item">
                        <img src="<?php echo base_url()."img/team/6.jpg" ?>" class="img-responsive">
                </div>
                
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="<?php echo base_url() . "assets/vendor/jquery/jquery.min.js" ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url() . "assets/vendor/bootstrap/js/bootstrap.min.js" ?>"></script>

    <!-- Plugin JavaScript -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> -->
    <script src="<?php echo base_url() . "assets/vendor/jquery/jquery.easing.min.js" ?>"></script>

    <!-- Contact Form JavaScript -->
    <script src="<?php echo base_url() . "assets/js/jqBootstrapValidation.js" ?>"></script>
    <script src="<?php echo base_url() . "assets/js/contact_me.js" ?>"></script>

    <!-- Theme JavaScript -->
    <script src="<?php echo base_url() . "assets/js/freelancer.min.js" ?>"></script>

    <script type="text/javascript">
        function validateForm() {
            var a = document.forms["loginForm"]["name"].value;
            var b = document.forms["loginForm"]["jurusan"].value;
            var c = document.forms["loginForm"]["institusi"].value;
            var d = document.forms["loginForm"]["jk"].value;
            var e = document.forms["loginForm"]["optradio"].value;
            var f = document.forms["loginForm"]["optradio1"].value;
            var g = document.forms["loginForm"]["optradio2"].value;
            if (a == null || a == "" || b == null || b == "" || c == null || c == "" || d == null || d == "" || e == null || e == "" || f == null || f == "" || g == null || g == "" ) {
                alert("Harap lengkapi formulir");
                return false;
            }
        }
        $(document).ready(function() {
            
            
        });
    </script>

</body>

</html>