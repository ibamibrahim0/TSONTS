<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>To see or not to see</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url() . "assets/vendor/bootstrap/css/bootstrap.min.css" ?>">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url() . "assets/css/freelancer.css" ?>">

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="<?php echo base_url() . "assets/vendor/font-awesome/css/font-awesome.min.css" ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
<div id="skipnav"><a href="#maincontent">Skip to main content</a></div>

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="<?php echo base_url() . "index.php/welcome/index" ?>">To see or not to see</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header style="background-color:#fff; color:#000;">
        <div class="container" style="padding: 0px;">
            <div class="row" style="margin-top:120px;">
                <div class="col-lg-12" id="soal">
                    <h4>Report Kel. Soal 1</h4>
                    <table class="table table-bordered">
                        <thead>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Jurusan</th>
                            <th>Institusi</th>
                            <th>Jenis Kelamin</th>
                            <th>Sim</th>
                            <th>Pengalaman Berkendara</th>
                            <th>Frekuensi Dalam 1 Minggu</th>
                            <th>Detail Jawaban</th>
                        </thead>
                        <tbody style="text-align:center;">
                            <?php 
                                foreach($resp_1 as $res){
                                    echo "<tr>";
                                        echo "<td>".$res['nomor']."</td>";
                                        echo "<td>".$res['name']."</td>";
                                        echo "<td>".$res['jurusan']."</td>";
                                        echo "<td>".$res['institusi']."</td>";
                                        echo "<td>".$res['jk']."</td>";
                                        echo "<td>".$res['sim']."</td>";
                                        echo "<td>".$res['pengalaman']."</td>";
                                        echo "<td>".$res['frekuensi']."</td>";
                                        echo "<td><input type='hidden' value='".$res['id']."' class='bi'><button class='btn btn-xs btn-primary det'>Detail</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </header>
    <header style="background-color:#fff; color:#000;">
        <div class="container" style="padding: 0px;">
            <div class="row">
                <div class="col-lg-12" id="soal">
                    <h4>Report Kel. Soal 2</h4>
                    <table class="table table-bordered">
                        <thead>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Jurusan</th>
                            <th>Institusi</th>
                            <th>Jenis Kelamin</th>
                            <th>Sim</th>
                            <th>Pengalaman Berkendara</th>
                            <th>Frekuensi Dalam 1 Minggu</th>
                            <th>Detail Jawaban</th>
                        </thead>
                        <tbody style="text-align:center;">
                            <?php 
                                foreach($resp_2 as $res){
                                    echo "<tr>";
                                        echo "<td>".$res['nomor']."</td>";
                                        echo "<td>".$res['name']."</td>";
                                        echo "<td>".$res['jurusan']."</td>";
                                        echo "<td>".$res['institusi']."</td>";
                                        echo "<td>".$res['jk']."</td>";
                                        echo "<td>".$res['sim']."</td>";
                                        echo "<td>".$res['pengalaman']."</td>";
                                        echo "<td>".$res['frekuensi']."</td>";
                                        echo "<td><input type='hidden' value='".$res['id']."' class='bi'><button class='btn btn-xs btn-primary det'>Detail</button></td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </header>
    <header style="background-color:#fff; color:#000;">
        <div class="container" style="padding: 0px;">
            <div class="row">
                <div class="col-lg-12" id="soal">
                    <h4>Report Kel. Soal 3</h4>
                    <table class="table table-bordered">
                        <thead>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Jurusan</th>
                            <th>Institusi</th>
                            <th>Jenis Kelamin</th>
                            <th>Sim</th>
                            <th>Pengalaman Berkendara</th>
                            <th>Frekuensi Dalam 1 Minggu</th>
                            <th>Detail Jawaban</th>
                        </thead>
                        <tbody style="text-align:center;">
                            <?php 
                                if(isset($resp_3)){
                                    foreach($resp_3 as $res){
                                    echo "<tr>";
                                        echo "<td>".$res['nomor']."</td>";
                                        echo "<td>".$res['name']."</td>";
                                        echo "<td>".$res['jurusan']."</td>";
                                        echo "<td>".$res['institusi']."</td>";
                                        echo "<td>".$res['jk']."</td>";
                                        echo "<td>".$res['sim']."</td>";
                                        echo "<td>".$res['pengalaman']."</td>";
                                        echo "<td>".$res['frekuensi']."</td>";
                                        echo "<td><input type='hidden' value='".$res['id']."' class='bi'><button class='btn btn-xs btn-primary det'>Detail</button></td>";
                                    echo "</tr>";
                                }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </header>

    <header style="background-color:#fff; color:#000;">
        <div class="container" style="padding: 0px;">
            <div class="row">
                <div class="col-lg-12" id="soal">
                    <h4>Report Kel. Soal 4</h4>
                    <table class="table table-bordered">
                        <thead>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Jurusan</th>
                            <th>Institusi</th>
                            <th>Jenis Kelamin</th>
                            <th>Sim</th>
                            <th>Pengalaman Berkendara</th>
                            <th>Frekuensi Dalam 1 Minggu</th>
                            <th>Detail Jawaban</th>
                        </thead>
                        <tbody style="text-align:center;">
                            <?php
                                if(isset($resp_4)){
                                    foreach($resp_4 as $res){
                                    echo "<tr>";
                                        echo "<td>".$res['nomor']."</td>";
                                        echo "<td>".$res['name']."</td>";
                                        echo "<td>".$res['jurusan']."</td>";
                                        echo "<td>".$res['institusi']."</td>";
                                        echo "<td>".$res['jk']."</td>";
                                        echo "<td>".$res['sim']."</td>";
                                        echo "<td>".$res['pengalaman']."</td>";
                                        echo "<td>".$res['frekuensi']."</td>";
                                        echo "<td><input type='hidden' value='".$res['id']."' class='bi'><button class='btn btn-xs btn-primary det'>Detail</button></td>";
                                    echo "</tr>";
                                }
                                }                               
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </header>

    <div id="det">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                        <fieldset disable>
                                            
                                            <div class="">
                                                <table border="0">
                                                    <thead>
                                                        <!-- <tr>
                                                            <th>Kelompok</th>
                                                            <th>Set</th>
                                                        </tr> -->
                                                    </thead>
                                                    <tbody id="dettab"></tbody>
                                                    <!-- <p><font color="red">*Click to approve item</font></p> -->
                                                </table>
                                            </div>
                                            <br>
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                    <thead>
                                                        <tr>
                                                            <th width="500px">Soal</th>
                                                            <th>Jawaban</th>
                                                            <th>Waktu</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="detjwb"></tbody>
                                                    <!-- <p><font color="red">*Click to approve item</font></p> -->
                                                </table>
                                            </div>
                                        </fieldset>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>

    <!-- jQuery -->
    <script src="<?php echo base_url() . "assets/vendor/jquery/jquery.min.js" ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url() . "assets/vendor/bootstrap/js/bootstrap.min.js" ?>"></script>

    <!-- Plugin JavaScript -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> -->
    <script src="<?php echo base_url() . "assets/vendor/jquery/jquery.easing.min.js" ?>"></script>

    <!-- Contact Form JavaScript -->
    <script src="<?php echo base_url() . "assets/js/jqBootstrapValidation.js" ?>"></script>
    <script src="<?php echo base_url() . "assets/js/contact_me.js" ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/bpopup.js"></script>

    <!-- Theme JavaScript -->
    <script src="<?php echo base_url() . "assets/js/freelancer.min.js" ?>"></script>

    <script type="text/javascript">
        $( document ).ready(function() {
            var BASE_URL = "<?php echo base_url(); ?>";
            $('#det').hide("fast");
            $('#next').click(function(){
                $('#soal').hide("fast");
                $('#jawab').show("fast");
            });
            $('#jwb').click(function(){
                $('#jawab').hide("fast");
                $('#q').show("fast");
            });

            $(".det").click(function(){
                var id = $(this).siblings(".bi").val();
                // $("#det").bPopup();
                var uri = BASE_URL + "index.php/welcome/getdet";
                // console.log(bid);
                $.ajax({
                    url : uri,
                    type: "POST",
                    data:{
                        id : id
                    },
                    dataType : 'json',
                    success: function(msg){
                        var jwb = msg.jwb;
                        var soal = msg.soal;

                        var jwb1 = null;
                        var waktu1 = 0;
                        var jwb2 = null;
                        var waktu2 = 0;
                        var jwb3 = null;
                        var waktu3 = 0;
                        var jwb4 = null;
                        var waktu4 = 0;
                        var jwb5 = null;
                        var waktu5 = 0;
                        var jwb6 = null;
                        var waktu6 = 0;
                        var jwb7 = null;
                        var waktu7 = 0;
                        var jwb8 = null;
                        var waktu8 = 0;
                        var jwb9 = null;
                        var waktu9 = 0;
                        var jwb10 = null;
                        var waktu10 = 0;
                        var q1 = null;
                        var q2 = null;
                        var q3 = null;

                        var row = "";
                        
                        for(var idx in jwb){
                            // row += "<tr>";
                            // row += "<td>Kelompok ";
                            // row += jwb[idx].kel_soal;
                            // row += "<br>";
                            // row += "Set Soal ";
                            // row += jwb[idx].set_soal;
                            // row += "</td>";
                            // row += "</tr>";

                            jwb1 = jwb[idx].jawab1;
                            waktu1 = jwb[idx].waktu1;
                            jwb2 = jwb[idx].jawab2;
                            waktu2 = jwb[idx].waktu2;
                            jwb3 = jwb[idx].jawab3;
                            waktu3 = jwb[idx].waktu3;
                            jwb4 = jwb[idx].jawab4;
                            waktu4 = jwb[idx].waktu4;
                            jwb5 = jwb[idx].jawab5;
                            waktu5 = jwb[idx].waktu5;
                            jwb6 = jwb[idx].jawab6;
                            waktu6 = jwb[idx].waktu6;
                            jwb7 = jwb[idx].jawab7;
                            waktu7 = jwb[idx].waktu7;
                            jwb8 = jwb[idx].jawab8;
                            waktu8 = jwb[idx].waktu8;
                            jwb9 = jwb[idx].jawab9;
                            waktu9 = jwb[idx].waktu9;
                            jwb10 = jwb[idx].jawab10;
                            waktu10 = jwb[idx].waktu10;
                            q1 = jwb[idx].q1;
                            q2 = jwb[idx].q2;
                            q3 = jwb[idx].q3;
                        }
                        // $("#dettab").empty().append(row);

                        for(var idx in soal){
                            if(soal[idx].urutan == 1){
                                row += "<tr>";
                                row += "<td>";
                                row += "<img src='"+soal[idx].soal+"' class='img-responsive'>";
                                row += "</td>";
                                row += "<td>"+jwb1+"</td>";
                                row += "<td>"+waktu1+" Milidetik</td>";
                                row += "</tr>";
                            }
                            if(soal[idx].urutan == 2){
                                row += "<tr>";
                                row += "<td>";
                                row += "<img src='"+soal[idx].soal+"' class='img-responsive'>";
                                row += "</td>";
                                row += "<td>"+jwb2+"</td>";
                                row += "<td>"+waktu2+" Milidetik</td>";
                                row += "</tr>";
                            }
                            if(soal[idx].urutan == 3){
                                row += "<tr>";
                                row += "<td>";
                                row += "<img src='"+soal[idx].soal+"' class='img-responsive'>";
                                row += "</td>";
                                row += "<td>"+jwb3+"</td>";
                                row += "<td>"+waktu3+" Milidetik</td>";
                                row += "</tr>";
                            }
                            if(soal[idx].urutan == 4){
                                row += "<tr>";
                                row += "<td>";
                                row += "<img src='"+soal[idx].soal+"' class='img-responsive'>";
                                row += "</td>";
                                row += "<td>"+jwb4+"</td>";
                                row += "<td>"+waktu4+" Milidetik</td>";
                                row += "</tr>";
                            }
                            if(soal[idx].urutan == 5){
                                row += "<tr>";
                                row += "<td>";
                                row += "<img src='"+soal[idx].soal+"' class='img-responsive'>";
                                row += "</td>";
                                row += "<td>"+jwb5+"</td>";
                                row += "<td>"+waktu5+" Milidetik</td>";
                                row += "</tr>";
                            }
                            if(soal[idx].urutan == 6){
                                row += "<tr>";
                                row += "<td>";
                                row += "<img src='"+soal[idx].soal+"' class='img-responsive'>";
                                row += "</td>";
                                row += "<td>"+jwb6+"</td>";
                                row += "<td>"+waktu6+" Milidetik</td>";
                                row += "</tr>";
                            }
                            if(soal[idx].urutan == 7){
                                row += "<tr>";
                                row += "<td>";
                                row += "<img src='"+soal[idx].soal+"' class='img-responsive'>";
                                row += "</td>";
                                row += "<td>"+jwb7+"</td>";
                                row += "<td>"+waktu7+" Milidetik</td>";
                                row += "</tr>";
                            }
                            if(soal[idx].urutan == 8){
                                row += "<tr>";
                                row += "<td>";
                                row += "<img src='"+soal[idx].soal+"' class='img-responsive'>";
                                row += "</td>";
                                row += "<td>"+jwb8+"</td>";
                                row += "<td>"+waktu8+" Milidetik</td>";
                                row += "</tr>";
                            }
                            if(soal[idx].urutan == 9){
                                row += "<tr>";
                                row += "<td>";
                                row += "<img src='"+soal[idx].soal+"' class='img-responsive'>";
                                row += "</td>";
                                row += "<td>"+jwb9+"</td>";
                                row += "<td>"+waktu9+" Milidetik</td>";
                                row += "</tr>";
                            }
                            if(soal[idx].urutan == 10){
                                row += "<tr>";
                                row += "<td>";
                                row += "<img src='"+soal[idx].soal+"' class='img-responsive'>";
                                row += "</td>";
                                row += "<td>"+jwb10+"</td>";
                                row += "<td>"+waktu10+" Milidetik</td>";
                                row += "</tr>";
                            }
                        }
                        $("#detjwb").empty().append(row);
                        $("#det").bPopup();
                    },
                    error:function(jqXHR,textStatus, errorThrown) {
                        alert("Error !!!");
                    }
                });
            });
        });
    </script>

</body>

</html>