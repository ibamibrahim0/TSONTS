<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('welcome_model','mod');
	}

	public function index()
	{
		$this->load->view('index');
	}
	public function pick()
	{
		$name = $this->input->post('name');
		$jurusan = $this->input->post('jurusan');
		$institusi = $this->input->post('institusi');
		$jk = $this->input->post('jk');
		$sim = $this->input->post('optradio');
		$pengalaman = $this->input->post('optradio1');
		$frekuensi = $this->input->post('optradio2');

		

		$res = $this->mod->regis($name,$jurusan,$institusi,$jk,$sim,$pengalaman,$frekuensi);

		// echo $res;
		$this->session->set_userdata("id",$res);

		$this->load->view('pick');
	}
	public function prevsoal1()
	{
		$set = rand(1,4);
		$klp = 1;
		$get_soal = $this->mod->get_set($set,$klp);
		$urutan = 0;

		$this->session->set_userdata("klp",$klp);
		$this->session->set_userdata("set",$set);

		// echo "<pre>";
		// print_r($get_soal);
		// echo "</pre>";

		foreach($get_soal as $res){
			
			if(!isset($data["soal"][$res->id_soal])){
				$data["soal"][$res->id_soal] = array();
				$data["soal"][$res->id_soal]["urutan"] = $urutan+1;
				$data["soal"][$res->id_soal]["soal"] = base_url()."img/soal/kel".$res->kel_soal."/".$res->nama;
			}
			$urutan++;
		}

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

		$this->load->view('soal',$data);
	}
	public function prevsoal2()
	{
		$set = rand(1,4);
		$klp = 2;
		$get_soal = $this->mod->get_set($set,$klp);
		$urutan = 0;

		$this->session->set_userdata("klp",$klp);
		$this->session->set_userdata("set",$set);

		// echo "<pre>";
		// print_r($get_soal);
		// echo "</pre>";

		foreach($get_soal as $res){
			
			if(!isset($data["soal"][$res->id_soal])){
				$data["soal"][$res->id_soal] = array();
				$data["soal"][$res->id_soal]["urutan"] = $urutan+1;
				$data["soal"][$res->id_soal]["soal"] = base_url()."img/soal/kel".$res->kel_soal."/".$res->nama;
			}
			$urutan++;
		}

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

		$this->load->view('soal',$data);
	}
	public function prevsoal3()
	{
		$set = rand(1,4);
		$klp = 3;
		$get_soal = $this->mod->get_set($set,$klp);
		$urutan = 0;

		$this->session->set_userdata("klp",$klp);
		$this->session->set_userdata("set",$set);

		// echo "<pre>";
		// print_r($get_soal);
		// echo "</pre>";

		foreach($get_soal as $res){
			
			if(!isset($data["soal"][$res->id_soal])){
				$data["soal"][$res->id_soal] = array();
				$data["soal"][$res->id_soal]["urutan"] = $urutan+1;
				$data["soal"][$res->id_soal]["soal"] = base_url()."img/soal/kel".$res->kel_soal."/".$res->nama;
			}
			$urutan++;
		}

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

		$this->load->view('soal',$data);
	}
	public function prevsoal4()
	{
		$set = rand(1,4);
		$klp = 4;
		$get_soal = $this->mod->get_set($set,$klp);
		$urutan = 0;

		$this->session->set_userdata("klp",$klp);
		$this->session->set_userdata("set",$set);

		// echo "<pre>";
		// print_r($get_soal);
		// echo "</pre>";

		foreach($get_soal as $res){
			
			if(!isset($data["soal"][$res->id_soal])){
				$data["soal"][$res->id_soal] = array();
				$data["soal"][$res->id_soal]["urutan"] = $urutan+1;
				$data["soal"][$res->id_soal]["soal"] = base_url()."img/soal/kel".$res->kel_soal."/".$res->nama;
			}
			$urutan++;
		}

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

		$this->load->view('soal',$data);
	}

	public function jawab()
	{
		$soal1 = $this->input->post('soal1');
		$waktu1 = $this->input->post('waktu1');
		$soal2 = $this->input->post('soal2');
		$waktu2 = $this->input->post('waktu2');
		$soal3 = $this->input->post('soal3');
		$waktu3 = $this->input->post('waktu3');
		$soal4 = $this->input->post('soal4');
		$waktu4 = $this->input->post('waktu4');
		$soal5 = $this->input->post('soal5');
		$waktu5 = $this->input->post('waktu5');
		$soal6 = $this->input->post('soal6');
		$waktu6 = $this->input->post('waktu6');
		$soal7 = $this->input->post('soal7');
		$waktu7 = $this->input->post('waktu7');
		$soal8 = $this->input->post('soal8');
		$waktu8 = $this->input->post('waktu8');
		$soal9 = $this->input->post('soal9');
		$waktu9 = $this->input->post('waktu9');
		$soal10 = $this->input->post('soal10');
		$waktu10 = $this->input->post('waktu10');

		$res = $this->mod->jawaban($this->session->userdata("id"),$this->session->userdata("klp"),$this->session->userdata("set"),$soal1,$waktu1,$soal2,$waktu2,$soal3,$waktu3,$soal4,$waktu4,$soal5,$waktu5,$soal6,$waktu6,$soal7,$waktu7,$soal8,$waktu8,$soal9,$waktu9,$soal10,$waktu10);

		$this->session->unset_userdata('id');
		$this->session->unset_userdata('klp');
		$this->session->unset_userdata('set');

		echo json_encode("0");
	}
	public function report()
	{
		$get_resp_1 = $this->mod->getresponden(1);
		$get_resp_2 = $this->mod->getresponden(2);
		$get_resp_3 = $this->mod->getresponden(3);
		$get_resp_4 = $this->mod->getresponden(4);
		$urutan = 1;

		foreach($get_resp_1 as $res){
			
			if(!isset($data["resp_1"][$res->id])){
				$data["resp_1"][$res->id] = array();
				$data["resp_1"][$res->id]["nomor"] = $urutan;
				$data["resp_1"][$res->id]["id"] = $res->id;
				$data["resp_1"][$res->id]["name"] = $res->init;
				$data["resp_1"][$res->id]["jurusan"] = $res->jurusan;
				$data["resp_1"][$res->id]["jk"] = NULL;
				$data["resp_1"][$res->id]["institusi"] = $res->institusi;
				$data["resp_1"][$res->id]["sim"] = NULL;
				$data["resp_1"][$res->id]["pengalaman"] = NULL;
				$data["resp_1"][$res->id]["frekuensi"] = NULL;

				if($res->jk == 'lk')
					$data["resp_1"][$res->id]["jk"] = "Laki - Laki";
				else
					$data["resp_1"][$res->id]["jk"] = "Perempuan";

				if($res->sim == 0)
					$data["resp_1"][$res->id]["sim"] = "Ya";
				else
					$data["resp_1"][$res->id]["sim"] = "Tidak";

				if($res->pengalaman == 0)
					$data["resp_1"][$res->id]["pengalaman"] = "Kurang dari 1 tahun";
				else
					$data["resp_1"][$res->id]["pengalaman"] = "Lebih dari 1 tahun";

				if($res->frekuensi == 0)
					$data["resp_1"][$res->id]["frekuensi"] = "Kurang dari 5 kali";
				else
					$data["resp_1"][$res->id]["frekuensi"] = "Lebih dari atau 5 kali";
			}
			$urutan++;
		}
		$urutan = 1;
		
		foreach($get_resp_2 as $res){
			
			if(!isset($data["resp_2"][$res->id])){
				$data["resp_2"][$res->id] = array();
				$data["resp_2"][$res->id]["nomor"] = $urutan;
				$data["resp_2"][$res->id]["id"] = $res->id;
				$data["resp_2"][$res->id]["name"] = $res->init;
				$data["resp_2"][$res->id]["jurusan"] = $res->jurusan;
				$data["resp_2"][$res->id]["jk"] = NULL;
				$data["resp_2"][$res->id]["institusi"] = $res->institusi;
				$data["resp_2"][$res->id]["sim"] = NULL;
				$data["resp_2"][$res->id]["pengalaman"] = NULL;
				$data["resp_2"][$res->id]["frekuensi"] = NULL;

				if($res->jk == 'lk')
					$data["resp_2"][$res->id]["jk"] = "Laki - Laki";
				else
					$data["resp_2"][$res->id]["jk"] = "Perempuan";

				if($res->sim == 0)
					$data["resp_2"][$res->id]["sim"] = "Ya";
				else
					$data["resp_2"][$res->id]["sim"] = "Tidak";

				if($res->pengalaman == 0)
					$data["resp_2"][$res->id]["pengalaman"] = "Kurang dari 1 tahun";
				else
					$data["resp_2"][$res->id]["pengalaman"] = "Lebih dari 1 tahun";

				if($res->frekuensi == 0)
					$data["resp_2"][$res->id]["frekuensi"] = "Kurang dari 5 kali";
				else
					$data["resp_2"][$res->id]["frekuensi"] = "Lebih dari atau 5 kali";
			}
			$urutan++;
		}
		$urutan = 1;

		foreach($get_resp_3 as $res){
			
			if(!isset($data["resp_3"][$res->id])){
				$data["resp_3"][$res->id] = array();
				$data["resp_3"][$res->id]["nomor"] = $urutan;
				$data["resp_3"][$res->id]["id"] = $res->id;
				$data["resp_3"][$res->id]["name"] = $res->init;
				$data["resp_3"][$res->id]["jurusan"] = $res->jurusan;
				$data["resp_3"][$res->id]["jk"] = NULL;
				$data["resp_3"][$res->id]["institusi"] = $res->institusi;
				$data["resp_3"][$res->id]["sim"] = NULL;
				$data["resp_3"][$res->id]["pengalaman"] = NULL;
				$data["resp_3"][$res->id]["frekuensi"] = NULL;

				if($res->jk == 'lk')
					$data["resp_3"][$res->id]["jk"] = "Laki - Laki";
				else
					$data["resp_3"][$res->id]["jk"] = "Perempuan";

				if($res->sim == 0)
					$data["resp_3"][$res->id]["sim"] = "Ya";
				else
					$data["resp_3"][$res->id]["sim"] = "Tidak";

				if($res->pengalaman == 0)
					$data["resp_3"][$res->id]["pengalaman"] = "Kurang dari 1 tahun";
				else
					$data["resp_3"][$res->id]["pengalaman"] = "Lebih dari 1 tahun";

				if($res->frekuensi == 0)
					$data["resp_3"][$res->id]["frekuensi"] = "Kurang dari 5 kali";
				else
					$data["resp_3"][$res->id]["frekuensi"] = "Lebih dari atau 5 kali";
			}
			$urutan++;
		}
		$urutan = 1;

		foreach($get_resp_4 as $res){
			
			if(!isset($data["resp_4"][$res->id])){
				$data["resp_4"][$res->id] = array();
				$data["resp_4"][$res->id]["nomor"] = $urutan;
				$data["resp_4"][$res->id]["id"] = $res->id;
				$data["resp_4"][$res->id]["name"] = $res->init;
				$data["resp_4"][$res->id]["jurusan"] = $res->jurusan;
				$data["resp_4"][$res->id]["jk"] = NULL;
				$data["resp_4"][$res->id]["institusi"] = $res->institusi;
				$data["resp_4"][$res->id]["sim"] = NULL;
				$data["resp_4"][$res->id]["pengalaman"] = NULL;
				$data["resp_4"][$res->id]["frekuensi"] = NULL;

				if($res->jk == 'lk')
					$data["resp_4"][$res->id]["jk"] = "Laki - Laki";
				else
					$data["resp_4"][$res->id]["jk"] = "Perempuan";

				if($res->sim == 0)
					$data["resp_4"][$res->id]["sim"] = "Ya";
				else
					$data["resp_4"][$res->id]["sim"] = "Tidak";

				if($res->pengalaman == 0)
					$data["resp_4"][$res->id]["pengalaman"] = "Kurang dari 1 tahun";
				else
					$data["resp_4"][$res->id]["pengalaman"] = "Lebih dari 1 tahun";

				if($res->frekuensi == 0)
					$data["resp_4"][$res->id]["frekuensi"] = "Kurang dari 5 kali";
				else
					$data["resp_4"][$res->id]["frekuensi"] = "Lebih dari atau 5 kali";
			}

			$urutan++;
		}

		$this->load->view('report',$data);
	}

	public function getdet()
	{
		$id = $this->input->post('id');
		$set = 0;
		$klp = 0;
		$urutan = 0;

		$jwb = $this->mod->getjwb($id);

		foreach($jwb as $res){
			$set = $res->set_soal;
			$klp = $res->kel_soal;
		}

		$data['jwb'] = $jwb;

		$get_soal = $this->mod->get_set($set,$klp);
		foreach($get_soal as $res){
			
			if(!isset($data["soal"][$res->id_soal])){
				$data["soal"][$res->id_soal] = array();
				$data["soal"][$res->id_soal]["urutan"] = $urutan+1;
				$data["soal"][$res->id_soal]["soal"] = base_url()."img/soal/kel".$res->kel_soal."/".$res->nama;
			}
			$urutan++;
		}

		echo json_encode($data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */