<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_model extends CI_Model {

	function loginval($uname,$pword){

		$this->db->where('username', $uname);
		$this->db->where('password', $pword);
		$this->db->from('user_db');
		return $this->db->count_all_results();
	}

	function get_lvl($uname){

		$this->db->select('level');
		$this->db->where('username', $uname);
		$res = $this->db->get('user_db');
		return $res->result();
	}

	function get_fn($uname){

		$this->db->select('fullname,user_id');
		$this->db->where('username', $uname);
		$res = $this->db->get('user_db');
		return $res->result();
	}

	function get_dept($uname){

		$this->db->select('department');
		$this->db->where('username', $uname);
		$res = $this->db->get('user_db');
		return $res->result();
	}

	function getitem($dept){

		// $this->db->where('dept', $dept);
		$this->db->order_by("itemid", "asc");
		$res = $this->db->get('itemdb');
		return $res->result();
	}

	function getitemall(){

		$res = $this->db->get('itemdb');
		return $res->result();
	}

	function getiddept($dept){

		$this->db->select('dept_id');
		$this->db->from('dept_db');
		$this->db->where('dept_name', $dept);
		$res = $this->db->get();
		
		return $res->result();
	}

	function getidtype($type){

		$this->db->select('id');
		$this->db->where('item_type', $type);
		$res = $this->db->get('type_db');
		return $res->result();
	}

	function getdeptlist(){

		// $this->db->select('dept_name');
		$res = $this->db->get('dept_db');
		return $res->result();
	}

	function getdeptname($dept){

		$this->db->select('dept_name');
		$this->db->from('dept_db');
		$this->db->where('dept_id', $dept);
		$res = $this->db->get();
		
		return $res->result();
	}

	function getlastcount($dept){

		$this->db->select('itemid');
		$this->db->from('itemdb');
		$this->db->like('itemid',$dept,'after');
		// $this->db->where('dept', $dept);
		$this->db->order_by("itemid", "desc");
		$this->db->limit(1);
		$res = $this->db->get();
		
		return $res->result();
	}

	function getsuplist(){

		// $this->db->select('dept_name');
		$res = $this->db->get('mssupplier');
		return $res->result();
	}

	function getbor($bid){

		$this->db->select('id.itemid,id.itemname,tb.reqqty,id.sat');
		$this->db->from('tr_borrow tb');
		$this->db->join('itemdb id', 'tb.itemid = id.id');
		$this->db->where('tb.borrowid', $bid);
		$res = $this->db->get();

		return $res->result();
	}

	function getinst($iid){

		$this->db->select('id.id,tm.itemid,tm.itemname,tm.qty');
		$this->db->from('tr_masuk tm');
		$this->db->join('itemdb id', 'tm.itemid = id.itemid');
		$this->db->where('insertid', $iid);
		$res = $this->db->get();

		return $res->result();
	}

	function getalamatsup($sup){

		$this->db->select('alamat');
		$this->db->from('mssupplier');
		$this->db->where('supname', $sup);
		$res = $this->db->get();

		return $res->result();
	}

	function getitemname($itemid){

		$this->db->select('itemname');
		$this->db->from('itemdb');
		$this->db->where('itemid', $itemid);
		$res = $this->db->get();

		return $res->result();
	}

	function detitem($itemid){

		$this->db->select('*');
		$this->db->from('itemdb');
		$this->db->where('itemid', $itemid);
		$res = $this->db->get();

		return $res->result();
	}

	function detitembyid($itemid){

		$this->db->select('*');
		$this->db->from('itemdb');
		$this->db->where('id', $itemid);
		$res = $this->db->get();

		return $res->result();
	}

	function gettypeall(){

		// $this->db->select('*');
		$this->db->from('type_db');
		$res = $this->db->get();

		return $res->result();
	}

	function getunitall(){
		$this->db->distinct();
		$this->db->select('unit');
		$this->db->from('unit_db');
		$res = $this->db->get();

		return $res->result();
	}

	function getinstdet($iid){

		$this->db->select('tm.itemid,tm.itemname,tm.qty,id.sat');
		$this->db->from('tr_masuk tm');
		$this->db->join('itemdb id', 'tm.itemid = id.itemid','left');
		$this->db->where('tm.insertid', $iid);
		$res = $this->db->get();

		return $res->result();
	}

	function getsup(){

		$this->db->select('supname,alamat,telepon,email,cperson');
		$this->db->from('mssupplier');
		$res = $this->db->get();

		return $res->result();
	}

	function reqitemdet($bid){

		$this->db->select('tb.borrowid,tb.itemid,id.itemname,tb.reqqty,id.sat');
		$this->db->from('tr_borrow tb');
		$this->db->join('itemdb id', 'tb.itemid = id.id');
		$this->db->where('tb.borrowid', $bid);
		$res = $this->db->get();

		return $res->result();
	}

	function reqitemdetall($bid){

		$this->db->select('tb.borrowid,id.itemid,id.itemname,tb.reqqty,id.sat,id.qty,tb.resqty');
		$this->db->from('tr_borrow tb');
		$this->db->join('itemdb id', 'tb.itemid = id.id');
		$this->db->where('tb.borrowid', $bid);
		$res = $this->db->get();

		return $res->result();
	}

	function getreq($name){

		$this->db->select('ud.fullname as nama,ta.waktu,ta.dept,ta.status,ta.borrowid,');
		$this->db->from('tr_approve ta');
		$this->db->join('user_db ud', 'ta.borrowerid = ud.user_id');
		$this->db->where('ta.borrowerid', $name);
		$res = $this->db->get();

		return $res->result();
	}

	function getdetapp($name){

		$this->db->select('ud.fullname,ta.dept');
		$this->db->from('tr_approve ta');
		$this->db->join('user_db ud', 'ta.borrowerid = ud.user_id');
		// $this->db->join('tr_approve ta', 'tb.borrowid = ta.borrowid');
		$this->db->where('ta.borrowid', $name);
		$res = $this->db->get();

		return $res->result();
	}

	function getreqall(){

		$this->db->select('ud.fullname as nama,ta.waktu,ta.dept,ta.status,ta.borrowid,');
		$this->db->from('tr_approve ta');
		$this->db->join('user_db ud', 'ta.borrowerid = ud.user_id');
		
		$res = $this->db->get();

		return $res->result();
	}

	function getbarangmasuk(){

		$this->db->select('*');
		$this->db->from('tr_masuk');
		
		$res = $this->db->get();

		return $res->result();
	}

	function getuserall(){

		$this->db->select('username,fullname,department,level');
		$this->db->from('user_db');
		$res = $this->db->get();

		return $res->result();
	}

	function getqty($itemid){

		$this->db->select('qty');
		$this->db->from('itemdb');
		$this->db->where('itemid', $itemid);
		$res = $this->db->get();

		return $res->result();
	}

	function getqtyid($id){

		$this->db->select('qty');
		$this->db->from('itemdb');
		$this->db->where('id', $id);
		$res = $this->db->get();

		return $res->result();
	}

	function chk_item($itemid){

		$this->db->from('itemdb');
		$this->db->where('itemid', $itemid);
		$res = $this->db->get();

		return $res->result();
	}

	function getbid(){

		$this->db->select('borrowid');
		$this->db->order_by("id", "desc");
		$this->db->limit(1);
		$res = $this->db->get('tr_borrow');
		return $res->result();
	}

	function getiid(){

		$this->db->select('insertid');
		$this->db->order_by("id", "desc");
		$this->db->limit(1);
		$res = $this->db->get('tr_masuk');
		return $res->result();
	}

	function masuk($iname){

		$query = "
			Select waktu,tbqty,status,user,qty
			from
			(
				SELECT tb.waktu as waktu,tb.qty as tbqty,tb.status,tb.user,tb.currqty as qty
				FROM tr_masuk tb
				JOIN itemdb id ON tb.itemid = id.itemid
				WHERE tb.itemid = ?

				UNION

				SELECT tbr.waktu as waktu,tbr.resqty as tbqty,tbr.status,tbr.user,tbr.qty
				FROM tr_borrow tbr
				JOIN tr_approve ta ON tbr.borrowid = ta.borrowid
				JOIN itemdb id ON tbr.itemid = id.id
				WHERE id.itemid = ? AND ta.status = 1
			) AS result

			ORDER BY waktu
        ";

   //      $query = "
			// Select waktu,tbqty,status,user,qty
			// from
			// (
			// 	SELECT tb.waktu as waktu,tb.qty as tbqty,tb.status,tb.user,tb.currqty as qty
			// 	FROM tr_masuk tb
			// 	JOIN itemdb id ON tb.itemid = id.itemid
			// 	WHERE MONTH(tb.waktu) = MONTH(CURDATE()) AND tb.itemid = ?

			// 	UNION

			// 	SELECT tbr.waktu as waktu,tbr.resqty as tbqty,tbr.status,tbr.user,tbr.qty
			// 	FROM tr_borrow tbr
			// 	JOIN tr_approve ta ON tbr.borrowid = ta.borrowid
			// 	JOIN itemdb id ON tbr.itemid = id.id
			// 	WHERE MONTH(tbr.waktu) = MONTH(CURDATE()) AND id.itemid = ? AND ta.status = 1
			// ) AS result

			// ORDER BY waktu
   //      ";

		$bind = array(
			$iname,
			$iname
		);

		return $this->db->query($query,$bind)->result();
	}

	// function keluar($iname){

	// 	$query = "
	// 			SELECT tb.waktu,tb.resqty as resqty
	// 			FROM tr_borrow tb
	// 			JOIN itemdb id ON tb.itemid = id.id
	// 			WHERE MONTH(tb.waktu) = MONTH(CURDATE()) AND id.itemid = ?
	// 	";

	// 	$bind = array(
	// 		$iname
	// 	);

	// 	return $this->db->query($query,$bind)->result();
	// }

	function reqitem($bid,$iname,$qty,$date){

		$query = "
				INSERT INTO tr_borrow(borrowid,itemid,reqqty,resqty,tanggal)
				VALUES(?,?,?,'',?)
		";

		$bind = array(
			$bid,
			$iname,
			$qty,
			$date
		);

		return $this->db->query($query,$bind);
	}

	function periodbarangkeluar($iditem,$stockawal,$qty,$stockakhir){

		$query = "
				INSERT INTO itemperiod(itemid,stockawal,qty,stockakhir,status)
				VALUES(?,?,?,?,'1')
		";

		$bind = array(
			$iditem,
			$stockawal,
			$qty,
			$stockakhir
		);

		return $this->db->query($query,$bind);
	}

	function periodbarangmasuk($iditem,$stockawal,$qty,$stockakhir){

		$query = "
				INSERT INTO itemperiod(itemid,stockawal,qty,stockakhir,status)
				VALUES(?,?,?,?,'2')
		";

		$bind = array(
			$iditem,
			$stockawal,
			$qty,
			$stockakhir
		);

		return $this->db->query($query,$bind);
	}

	function insertitem($iid,$iname,$qty,$kitem,$curr_qty){

		$query = "
				INSERT INTO tr_masuk(insertid,itemid,itemname,qty,currqty)
				VALUES(?,?,?,?,?)
		";

		$bind = array(
			$iid,
			$kitem,
			$iname,
			$qty,
			$curr_qty
		);

		return $this->db->query($query,$bind);
	}

	function addsup($sup,$supadd,$telepon,$email,$cperson){

		$query = "
				INSERT INTO mssupplier(supname,alamat,telepon,email,cperson)
				VALUES(?,?,?,?,?)
		";

		$bind = array(
			$sup,
			$supadd,
			$telepon,
			$email,
			$cperson
		);

		return $this->db->query($query,$bind);
	}

	// function regis($uname,$pwd,$fname,$dept){

	// 	$query = "
	// 			INSERT INTO user_db(username,password,department,fullname,level)
	// 			VALUES(?,?,?,?,'Employee')
	// 	";

	// 	$bind = array(
	// 		$uname,
	// 		$pwd,
	// 		$dept,
	// 		$fname
	// 	);

	// 	return $this->db->query($query,$bind);
	// }

	function regisadv($uname,$pwd,$fname,$level){

		$query = "
				INSERT INTO user_db(username,password,department,fullname,level)
				VALUES(?,?,'',?,?)
		";

		$bind = array(
			$uname,
			$pwd,
			$fname,
			$level
		);

		return $this->db->query($query,$bind);
	}

	function del($uname){

		$query = "
				DELETE FROM user_db
				WHERE username = ?
		";

		$bind = array(
			$uname
		);

		return $this->db->query($query,$bind);
	}

	// function inst_approve($nama,$dept,$bid){

	// 	$query = "
	// 			INSERT INTO tr_approve(borrowid,borrowerid,nama,dept)
	// 			VALUES(?,?,?,?)
	// 	";

	// 	$bind = array(
	// 		$bid,
	// 		$this->session->userdata("uname"),
	// 		$nama,
	// 		$dept
	// 	);

	// 	return $this->db->query($query,$bind);
	// }

	function inst_approve($bid,$id,$dept,$date){

		$query = "
				INSERT INTO tr_approve(borrowid,borrowerid,dept,waktu)
				VALUES(?,?,?,?)
		";

		$bind = array(
			$bid,
			$id,
			$dept,
			$date
		);

		return $this->db->query($query,$bind);
	}

	function inst_next($nama,$dept,$iid,$status){

		$query = "
				INSERT INTO tr_masuk_next(insertid,userid,nama,dept,status)
				VALUES(?,?,?,?,?)
		";

		$bind = array(
			$iid,
			$this->session->userdata("uname"),
			$nama,
			$dept,
			$status
		);

		return $this->db->query($query,$bind);
	}

	function insert_item($itemid,$itemname,$qty,$dept,$type=NULL,$unit=NULL,$price,$totprice){

		$query = "
				INSERT INTO itemdb(itemid,dept,itemname,minqty,qty,sat,fa,lokasi,harga,totharga)
				VALUES(?,?,?,0,?,?,'NON FA','LOG',?,?)
		";

		$bind = array(
			$itemid,
			$dept,
			// $type,
			$itemname,
			$qty,
			$unit,
			$price,
			$totprice
		);

		return $this->db->query($query,$bind);
	}

	function update_item($itemid,$qty){

		$query = "
				UPDATE itemdb SET qty = ?
				WHERE itemid = ?
		";

		$bind = array(
			$qty,
			$itemid
		);

		return $this->db->query($query,$bind);
	}

	function up_item($id,$qty,$bid,$harga){

		$query = "
				UPDATE itemdb SET qty = ?
				WHERE id = ?
		";

		$bind = array(
			$qty,
			$id
		);

		$this->db->query($query,$bind);

		$queryz = "
				UPDATE itemdb SET totharga = ?
				WHERE id = ?
		";

		$bindz = array(
			$harga,
			$id
		);

		$this->db->query($queryz,$bindz);

		$querys = "
				UPDATE tr_borrow SET qty = ?
				WHERE borrowid = ? AND itemid = ?
		";

		$bind = array(
			$qty,
			$bid,
			$id
		);

		return $this->db->query($querys,$bind);
	}

	function update_qty($itemid,$qty,$bid){

		$query = "
				UPDATE tr_borrow SET resqty = ?
				WHERE itemid = ? AND borrowid = ?
		";

		$bind = array(
			$qty,
			$itemid,
			$bid
		);

		return $this->db->query($query,$bind);
	}

	function approve($bid){

		$query = "
				UPDATE tr_approve SET status = 1
				WHERE borrowid = ?
		";

		$bind = array(
			$bid
		);

		return $this->db->query($query,$bind);
	}

	function dtp($sdate,$edate){

		// $query = "
		// 	Select waktu,tbqty,status,user,qty,itemid,itemname
		// 	from
		// 	(
		// 		SELECT tb.waktu as waktu,tb.qty as tbqty,tb.status,tb.user,tb.currqty as qty,tb.itemname as itemname,tb.itemid as itemid
		// 		FROM tr_masuk tb
		// 		JOIN itemdb id ON tb.itemid = id.itemid
		// 		WHERE tb.waktu BETWEEN ? AND ?

		// 		UNION

		// 		SELECT tbr.waktu as waktu,tbr.resqty as tbqty,tbr.status,tbr.user,tbr.qty,id.itemname as itemname,id.id as itemid
		// 		FROM tr_borrow tbr
		// 		JOIN tr_approve ta ON tbr.borrowid = ta.borrowid
		// 		JOIN itemdb id ON tbr.itemid = id.id
		// 		WHERE (tbr.waktu BETWEEN ? AND ?) AND ta.status = 1
		// 	) AS result

		// 	ORDER BY waktu
  //       ";

		// $bind = array(
		// 	$sdate,
		// 	$edate,
		// 	$sdate,
		// 	$edate
		// );

		$query = "
			SELECT *
			FROM itemperiod
			WHERE waktu BETWEEN ? and ?
        ";

		$bind = array(
			$sdate,
			$edate
		);

		return $this->db->query($query,$bind)->result();
	}

	function getlastqty($itemid){

		$this->db->select('qty');
		$this->db->where('itemid', $itemid);
		$res = $this->db->get('itemdb');
		return $res->result();
	}

	function getprice($itemid){

		$this->db->select('totharga');
		$this->db->where('itemid', $itemid);
		$res = $this->db->get('itemdb');
		return $res->result();
	}

	function inststockopname($itemid,$qty,$istock,$selisih,$ket){

		$query = "
				INSERT INTO stockopname(itemid,qty,qtystck,selisih,keterangan)
				VALUES(?,?,?,?,?)
		";

		$bind = array(
			$itemid,
			$qty,
			$istock,
			$selisih,
			$ket
		);

		return $this->db->query($query,$bind);
	}

	function getitemstockupdate(){

		$this->db->select('tb.*,id.itemname');
		$this->db->from('stockopname tb');
		$this->db->join('itemdb id', 'tb.itemid = id.itemid');
		$res = $this->db->get();

		return $res->result();
	}

	function getbarangmasukord(){

		$this->db->select('*');
		$this->db->from('tr_masuk');
		$this->db->group_by('insertid');
		
		$res = $this->db->get();

		return $res->result();
	}

	function uptprice($itemid,$price,$atotprice){

		$query = "
				UPDATE itemdb SET harga = ? 
				WHERE itemid = ?
		";

		$bind = array(
			$price,
			$itemid
		);

		$this->db->query($query,$bind);

		$querys = "
				UPDATE itemdb SET totharga = ? 
				WHERE itemid = ?
		";

		$binda = array(
			$atotprice,
			$itemid
		);

		return $this->db->query($querys,$binda);
	}

	function getitemexcel(){

		$this->db->select('itemid,itemname,itemtype,dept,qty,sat,harga,totharga,fa,lokasi');
		$this->db->from('itemdb');
		$res = $this->db->get();
		return $res->result();
	}

	function gethargaid($id){

		$this->db->select('totharga');
		$this->db->from('itemdb');
		$this->db->where('id', $id);
		$res = $this->db->get();

		return $res->result();
	}

	function getharga($id){

		$this->db->select('totharga');
		$this->db->from('itemdb');
		$this->db->where('itemid', $id);
		$res = $this->db->get();

		return $res->result();
	}

	function inst_baru($bid,$id,$dept){

		$query = "
				INSERT INTO tr_approve(borrowid,borrowerid,dept)
				VALUES(?,?,?)
		";

		$bind = array(
			$bid,
			$id,
			$dept
		);

		return $this->db->query($query,$bind);
	}

	function regis($name,$jurusan,$institusi,$jk,$sim,$pengalaman,$frekuensi){

		$query = "
				INSERT INTO reg(init,jurusan,jk,institusi,sim,pengalaman,frekuensi)
				VALUES(?,?,?,?,?,?,?)
		";

		$bind = array(
			$name,
			$jurusan,
			$jk,
			$institusi,
			$sim,
			$pengalaman,
			$frekuensi
		);

		$this->db->query($query,$bind);

		return $this->db->insert_id();
	}

	function get_set($set,$klp){

		$this->db->select('tb.id_soal,tb.kel_soal,id.nama');
		$this->db->from('kel_soal tb');
		$this->db->join('soal id', 'tb.id_soal = id.id');
		$this->db->where('tb.klp', $set);
		$this->db->where('id.kel', $klp);
		$res = $this->db->get();

		return $res->result();
	}

	function jawaban($name,$klp,$set,$soal1,$waktu1,$soal2,$waktu2,$soal3,$waktu3,$soal4,$waktu4,$soal5,$waktu5,$soal6,$waktu6,$soal7,$waktu7,$soal8,$waktu8,$soal9,$waktu9,$soal10,$waktu10){

		$query = "
				INSERT INTO jawab(user,kel_soal,set_soal,jawab1,waktu1,jawab2,waktu2,jawab3,waktu3,jawab4,waktu4,jawab5,waktu5,jawab6,waktu6,jawab7,waktu7,jawab8,waktu8,jawab9,waktu9,jawab10,waktu10)
				VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
		";

		$bind = array(
			$name,
			$klp,
			$set,
			$soal1,
			$waktu1,
			$soal2,
			$waktu2,
			$soal3,
			$waktu3,
			$soal4,
			$waktu4,
			$soal5,
			$waktu5,
			$soal6,
			$waktu6,
			$soal7,
			$waktu7,
			$soal8,
			$waktu8,
			$soal9,
			$waktu9,
			$soal10,
			$waktu10
		);

		return $this->db->query($query,$bind);
	}

	function getresponden($kelompok_soal){

		$this->db->select('id.*');
		$this->db->from('jawab tb');
		$this->db->join('reg id', 'tb.user = id.id AND tb.kel_soal='.$kelompok_soal);
		$res = $this->db->get();

		return $res->result();
	}
	function getjwb($id){

		$this->db->select('tb.*');
		$this->db->from('jawab tb');
		$this->db->where('tb.user', $id);
		$res = $this->db->get();

		return $res->result();
	}
}
